// Generatorls!


struct EvenNumbers: GeneratorType {
    var current = 0
    mutating func next() -> Int?{
        current = current + 2
        return current
    }
    
}

var evens = EvenNumbers()
evens.next()
evens.next()

// Deberes: Hacer una estructura llamada CommandArguments
// que implementa GeneratorType y usando Process.arguments
// te va dando los argumentos de la linea de comandos de uno
// en uno. Pruébalo con una app de linea de comandos en 
// Swift
Process.arguments