// Playground - noun: a place where people can play



// EMPAQUETADO
// Puedo empaquetar lo que quiera dentro de
// un Optional
var a: Int? = 42
var b: Int?
var c = Optional("Hola Mundo")

// Incluso otro opcional!
var d: String?? = c


// DESEMPAQUETADO

// De forma sensata
if let hola = c{
    // si había algo en Some, ahora está en 
    // en hola
    println(hola)
}

// Por cojones
println(c!) // pide a Jobs que haya algo ahí dentro...


// Optional Chaining
a?.successor()

// Luego veremso más cosas de los opcionales y su relación con Cocoa
