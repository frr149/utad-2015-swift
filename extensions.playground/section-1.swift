// Extensiones

typealias Task = () -> ()

extension Int{
    
    func times(task: Task){
        
        for _ in 1...self{
            task()
        }
    }
}

2.times{
    println("Hola, holita, vecinito")
}

extension Bool{
    
    // método ifTrue
    // solo ejecuta el task que recibe si self es verdadero
    // método ifFalse: pos más de lo mismo
    
    
    // método until
    // mientras self es verdadero, se va ejejcutando el task
    // aplícalo a una expresión que pueda ir cambiando
    
}