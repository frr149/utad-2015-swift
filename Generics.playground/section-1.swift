// Generics
// Sintaxis similar a Java & C++ en el caso de:
// * agregados (structs, enums y clases)
// * funciones
//
// Para Protocolos, se usa una sintaxis y concepto distinto
// llamado Associated Type
//

// Funciones
func descambiar<T>(inout a: T, inout b:T){
    // T es el tipo genérico sin restricciones
    var aux: T = a
    a = b
    b = aux
}

var (name, surname) = ("Anakin", "Skywalker")
name
surname
descambiar(&name, &surname)
name
surname

// Sobrecarga de funciones genéricas
// Aporto yo la versión específica para un tipo
// basta con definirla con el tipo correcto
func descambiar(inout a:Int, inout b:Int){
    println("Esta es la mia")
    var aux = a
    a = b
    b = aux
}

var (n,m) = (2,3)
descambiar(&n, &m)
n
m

// Tipos genéricos
struct Pair<T:Hashable, U>{
    var key: T
    var value: U
}

var p = Pair(key: "hola", value: Array<Int>())





// Agregados genéricos
struct Stack<T>{
    
    var _stack = Array<T>()
    
    // Accesores
    func peek() ->T{
        return _stack[0]
    }
    
    
    // Mutadores
    mutating func push(a:T) {
        _stack.insert(a, atIndex: 0)
    }
    
    mutating func pop() -> T{
        var head = _stack[0]
        _stack.removeAtIndex(0)
        return head
    }
}

// Deberes: hacer una pila segura. Si hago un pop en una
// pila vacía, se jode el invento.

// Protocolos genéricos
// Protocolos
protocol StackType{
    
    typealias ElementType
    
    var count:Int {get}
    
    mutating func push(item:ElementType)
    mutating func pop()->ElementType
    func peek() ->ElementType
    
}

class IntStack: StackType {
    
    
    typealias ElementType = Int
    
    var _stack = Array<ElementType>()
    
    var count: Int{
        get{
            return _stack.count
        }
    }
    
    func push(item:ElementType){
        _stack.insert(item, atIndex: 0)
    }
    func pop()->ElementType{
        var head = _stack[0]
        _stack.removeAtIndex(0)
        return head
    }
    func peek() ->ElementType{
        return _stack[0]
    }
}

// Deberes: Repasar el protocolo genérico que acabamos de
// ver y hacer que Sentient (y sus subclases, Jedi y Sith) 
// implementen el protocolo StringLiteralConvertible para que
// var obi : Jedi = "Obi Wan Kenobi"
// me cree un jedi con la propiedad name = "Obi Wan Kenobi"
// y las demás propiedades con sus valores por defecto.



// SuperDeberes
// Crea una Enum genérica llamada Maybe. Debe de tener dos
// estados: Something y Nothing (será tu propio Optional)
// Haz una lista de las características de Optional
// e intenta replicarlas.
// Podrás hacer casi todas mediante la implementación de varios
// protocolos.
// * Meter algo (genérico) dentro del Maybe: un init
// * Sacar algo (genérico) de la caja: un método accesor.
// * imprimir su contenido: implica implementar printable
// * iniciarlo con nil: implica implementar NilLiteralConvertible
// * Replicar el optional chaining (pa nota):
//      * crear una función fmap(Maybe<T>, (T)->U) -> Maybe<U>
//      * crear el operador ?. para que llame a esa función tal y 
//          como hace Optional<T>


// Super Mega Deberes: una enum genérica para gestionar errores
// Se llamará Result y tendrá dos estados: el de éxito y el de fallo
// el de fallo tendrá info sobre el error.
// una función que devuelve un entero y que la pueda cagar, pasaría a
// devolver un Result<Int>. Si la caga, estará en el estado fallo y 
// podremos pedirle info sobre el error. Si no la caga, estará en
// el estado éxito y podremos seguir trabajando con ese Result.
// Moraleja: es un Optional especializado para errores.
// Implementarlo es un ejercicio similar al anterior pero con un
// estado de "error" que guarda algo de información.
// Miraos el objeto Result de Rust: 
// http://www.hydrocodedesign.com/2014/05/28/practicality-with-rust-error-handling/
// Llegado a este punto eres un Swiftero de pro. 






















