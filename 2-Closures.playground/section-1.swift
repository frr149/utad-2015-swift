// Clausuras


// OJO, todas las funciones ya son clausuras, es sólo que les
// pareció guay inventar más sintaxis para lo mismo

// sintaxis habitual
func f (a:Int) ->Int {
    return a + 42
}

// Sintaxis de clausura
let f = {(a:Int) -> Int
        in
    return a + 3}

f(4)

// podemos poner clausuras en colecciones
var funcs = [f,
    {(a:Int) -> Int in return a * 2},   // sintaxis completa
    { a -> Int in return a * 33},       // tipo de parámetro inferido
    { a -> Int in a + 33},              // return implícito
    { a in a - 2},                      // tipo de retorno implícito
    { $0 * 3}
]

for f in funcs{
    f(1)
}

// Trailing closure: si el último parámetro de una función
// es otra función, la podemos llamar de forma algo diferente
typealias BinaryFun = (Int, Int) -> Int

func combine(a:Int, b:Int, combinator: BinaryFun) -> Int{
    return combinator(a,b)
}

// la llamo de forma normal
combine(3, 2, {$0 * $1})

// la llamo con la clausura que cuelga
combine(4, 5) { (a, b) -> Int in
    (a + 42) * (b - 1)
}









