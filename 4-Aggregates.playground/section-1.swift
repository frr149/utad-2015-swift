// Agregados: Enum, Structs, Classes, Tuplas


// Enums
enum LightSaberColor{
    case Red, Blue, Green, Purple
}

// Structs
struct LightSaber {
    
    // Class or static property
    static let quote = "An elegant weapon for a more civilized age"
    
    // Instance properties
    var color : LightSaberColor = .Blue {
        // This is an observer
        willSet{
            println("About to change color to \(color)")
        }
    }
    
    var isDoubleBladed = false
    
}

LightSaber.quote
var jediSaber = LightSaber()
jediSaber.color = .Green
var sithSaber = LightSaber(color: .Red, isDoubleBladed: true)


// Classes
class Sentient {
    
    var name = ""
    
    
    init(){}            // default init
    init(name:String){  // designated init
        self.name = name
    }
}

var jabba = Sentient(name:"Jabba the Hutt")


class Jedi: Sentient {
    
    // MARK : - Propiedades
    var midichlorians = 42
    var lightSaber = LightSaber(color: .Blue, isDoubleBladed: false)
    var padawanOf: Jedi?    // Añadimos Optionals. con esto ya es nil
    
    // MARK: - Init
    
    // Crea un Jedi completo
    convenience init(name: String, saber: LightSaber, midichlorians: Int){
        self.init(name: name)
        
        self.lightSaber = saber
        self.midichlorians = midichlorians
        
    }
    
    // Crea un maestro con 10K midiclorianos
    // demás props por defecto
    convenience init(masterName: String){
        self.init(name: masterName)
        midichlorians = 10000
    }
    
    
    // MARK: - Métodos normales
    func unsheathe() -> String{
        return "█||||||(•)█Ξ█████████████████████"
    }
    
}


var qui = Jedi(name: "Qui Gon Jinn",
    saber: LightSaber(color: .Green, isDoubleBladed: false), midichlorians: 4000);

var yoda = Jedi(masterName: "Mich Yoda")
qui.padawanOf = yoda

yoda.unsheathe()
qui.padawanOf?.name


class Sith: Jedi {
    
    // Crea un señor oscuro del Sith.
    // de conveniencia
    convenience init(darkLordName: String){
        self.init(masterName: darkLordName)
        lightSaber = LightSaber(color: .Red, isDoubleBladed: false)
        midichlorians = 10_000
    }
    
    
}

var vader = Sith(darkLordName: "Darth Vader")
vader.unsheathe()

var maul = Sith(darkLordName: "Darth Maul");


maul.lightSaber.isDoubleBladed =























