// Primeros pasos con Swift


// Variables y constantes
var hola = "Mundo"  // El compilador infiere el tipo por su valor
let answer : Int = 42 // El compilador no infiere una leche


// Todo es un objeto
Int.max
Double.abs(-45)



// Tuplas: asocian dos o más objetos
var q : (Int, String, String) = (42, "Cuarenta y dos", "fourtytwo")
q.0
q.1
q.2
q.1 = "Cuarentaidos"

//q.2 = 78


// La tuplas especiales:
// La 1-tupla no existe
let single = (8)    // es un ocho
// La tupla vacia representa la Nada
let nothing: () = ()

// Colecciones

// Array
var nums = [1,2,3,5] // no podría poner una cadena dentro
nums = nums + [6,7,8]

nums[0]
nums[4]

for elt in nums{
    println("Un elemento del array:\t\(elt+1)." )
}

// Dictionary
var bso = ["Barry" : ["Born Free", "Dancing with wolves"],
           "Sakamoto" : ["Black Rain"]];



bso["Barry"]
bso["Williams"]

for (keyword, value) in bso{
    
    println("\(keyword) ha compuesto: \(value)")
}

// funciones de andar por casa

// Solo nombre interno
func h(a:Int) -> Int{
    return a + 2
}
h(3)
//h(a:5)

func h1(elsa a: String) -> String{
    return "Elsa era una \(a)"
}

h1(elsa: "reina de Arendel")
h1(elsa: "leona")


func add(a:Int, b:Int, thenMultiplyBy c: Int) -> Int{
    return (a+b) * c
}

add(2, 5, thenMultiplyBy: 76)

// Funciones de primer nivel
// aceptar funciones como parámetros
// devolver funciones
typealias IntToIntFun = (Int)->Int

func apply(f:IntToIntFun, n:Int) -> Int{
    
    return f(n)
}
func addOne(a:Int) -> Int{
    return a.successor()
}
addOne(4)

apply(addOne, 6)


func compose(f:IntToIntFun, g:IntToIntFun) -> IntToIntFun{
    
    func comp(a:Int) -> Int{
        return g(f(a))
    }
    return comp
}

compose(addOne, h)(5)












